package exercises.ex4;

public interface PalindromeCheckerInterface {
   boolean isPalindrome(String theWord);
   void printResult();
}
